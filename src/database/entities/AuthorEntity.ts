import mongoose from "mongoose";

const AuthorSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    secondName: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    }
})

export const AuthorModel = mongoose.model("Authors", AuthorSchema);
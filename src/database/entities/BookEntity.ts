import mongoose from "mongoose";

const BookSchema = new mongoose.Schema({
    bookName: {
        type: String,
        required: true,
    },
    bookDepartment: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Departments",
        required: true
    },
    bookAuthor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Authors",
        required: true
    },
    rentalsNumber: {
        type: Number,
        required: true,
        default: 0,
    }
})

export const BookModel = mongoose.model("Books", BookSchema);
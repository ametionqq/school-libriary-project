import mongoose from "mongoose";

const HistorySchema = new mongoose.Schema({
    book: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Books",
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true,
    },
    rentalsDate: {
        type: String
    }
})

export const HistoryModel = mongoose.model("History", HistorySchema);
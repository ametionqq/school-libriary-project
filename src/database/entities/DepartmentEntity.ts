import mongoose from "mongoose";

const DepartmentSchema = new mongoose.Schema({
    departmentName: {
        type: String,
        required: true,
        unique: true
    },
    departmentCity: {
        type: String,
        required: true
    },
    booksAmount: {
        type: Number,
        required: true,
        default: 0
    }
})

export const DepartmentModel = mongoose.model("Departments", DepartmentSchema);
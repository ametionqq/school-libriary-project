import {composeWithMongoose} from "graphql-compose-mongoose";
import {BookModel} from "../database/entities/BookEntity";
import {schemaComposer} from "graphql-compose";
import {AuthorModel} from "../database/entities/AuthorEntity";
import {DepartmentModel} from "../database/entities/DepartmentEntity";
import {UserModel} from "../database/entities/UserEntity";
import {HistoryModel} from "../database/entities/HistoryEntity";

const BookSchema = composeWithMongoose(BookModel);
const AuthorSchema = composeWithMongoose(AuthorModel);
const DepartmentSchema = composeWithMongoose(DepartmentModel);
const UserSchema = composeWithMongoose(UserModel);
const HistorySchema = composeWithMongoose(HistoryModel);

schemaComposer.Query.addFields({
    findBookById: BookSchema.getResolver("findById"),
    findBook: BookSchema.getResolver("findOne"),
    findBooks: BookSchema.getResolver("findMany"),
    
    findAuthorById: AuthorSchema.getResolver("findById"),
    findAuthor: AuthorSchema.getResolver("findOne"),
    findAuthors: AuthorSchema.getResolver("findMany"),

    findDepartmentById: DepartmentSchema.getResolver("findById"),
    findDepartment: DepartmentSchema.getResolver("findOne"),
    findDepartments: DepartmentSchema.getResolver("findMany"),

    findUser: UserSchema.getResolver("findOne"),

    findHistory: HistorySchema.getResolver("findOne"),
})

schemaComposer.Mutation.addFields({
    addBook: BookSchema.getResolver("createOne"),
    addDepartment: DepartmentSchema.getResolver("createOne"),
    addAuthor: AuthorSchema.getResolver("createOne"),
    addUser: UserSchema.getResolver("createOne"),
    addHistory: HistorySchema.getResolver("createOne"),
    modifyHistory: HistorySchema.getResolver("updateOne")
})

BookSchema.addRelation("bookDepartment", {
    resolver: () => DepartmentSchema.getResolver("findById"),
    prepareArgs: {
        // @ts-ignore
        _id: (source) => source.bookDepartment
    },
    projection: { bookDepartment: 1 }
})

BookSchema.addRelation("bookAuthor", {
    resolver: () => AuthorSchema.getResolver("findById"),
    prepareArgs: {
        //@ts-ignore
        _id: (source) => source.bookAuthor
    },
    projection: { bookAuthor: 1 }
})

HistorySchema.addRelation("book", {
    resolver: () => BookSchema.getResolver("findById"),
    prepareArgs: {
        //@ts-ignore
        _id: (source) => source.book
    },
    projection: { book: 1 }
})

HistorySchema.addRelation("user", {
    resolver: () => UserSchema.getResolver("findById"),
    prepareArgs: {
        //@ts-ignore
        _id: (source) => source.user
    },
    projection: { user: 1 }
})

export const schema = schemaComposer.buildSchema()